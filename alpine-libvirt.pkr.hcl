variable "headless" {
  type    = bool
  default = true
}

variable "iso_url" {
  type    = string
  default = "https://dl-cdn.alpinelinux.org/alpine/v3.16/releases/x86_64/alpine-standard-3.16.2-x86_64.iso"
}

variable "iso_checksum" {
  type    = string
  default = "sha512:b4dcc47b28390c20348d14643863ce41fb9ad55755a1ec00af0117c3dd09c9d03c2c722b005534abece67baa2a017bd78a65c18d2dccd525491d679fe13cd615"
}

variable "disk_size" {
  type    = string
  default = "20G"
}

variable "ram" {
  type    = number
  default = 2048
}

variable "cpus" {
  type    = number
  default = 2
}

variable "rootfstype" {
  type    = string
  default = "ext4"
}

variable "ssh_username" {
  type    = string
  default = "kawaii"
}

variable "ssh_password" {
  type    = string
  default = "kawaii"
}

variable "vm_name" {
  type    = string
  default = "packer-alpine-stable-uefi-libvirt-x86_64"
}

source "qemu" "alpine-qemu-uefi" {
  accelerator = "kvm"
  boot_command = [
    "root<enter>",
    "ifconfig eth0 up && udhcpc -i eth0<enter><wait10s>",
    "wget -qO answers http://{{.HTTPIP}}:{{.HTTPPort}}/answers/alpine-libvirt<enter><wait5s>",
    "echo 'USEROPTS=\"-a -u ${var.ssh_username}\"' >> answers<enter>",
    "ERASE_DISKS=/dev/vda ROOTFS=${var.rootfstype} setup-alpine -ef answers<enter><wait10m>",
    "wget -qO- http://{{.HTTPIP}}:{{.HTTPPort}}/scripts/reset-uefi-grub.sh | sh -s /dev/vda<enter><wait30s>",
    "reboot<enter><wait40s>",
    "root<enter>",
    "echo '${var.ssh_username}:${var.ssh_password}' | chpasswd<enter>",
    "sed -i 's/^permit persist/permit nopass/' /etc/doas.d/doas.conf<enter>",
    "apk add dropbear-scp<enter>",
    "passwd -l root<enter>",
    "reboot<enter>"
  ]
  boot_wait        = "30s"
  disk_cache       = "none"
  disk_compression = true
  disk_discard     = "unmap"
  disk_interface   = "virtio"
  disk_size        = var.disk_size
  format           = "qcow2"
  headless         = var.headless
  http_directory   = "."
  iso_checksum     = var.iso_checksum
  iso_url          = var.iso_url
  net_device       = "virtio-net"
  output_directory = "artifacts/libvirt"
  qemu_binary      = "/usr/bin/qemu-system-x86_64"
  qemuargs = [
    ["-bios", "/usr/share/OVMF/OVMF.fd"],
    ["-m", "${var.ram}"],
    ["-display", "none"],
    ["-smp", "${var.cpus}"],
    ["-cpu", "host"]
  ]
  shutdown_command = "doas /sbin/poweroff"
  ssh_password     = var.ssh_password
  ssh_username     = var.ssh_username
  ssh_wait_timeout = "30m"
  vm_name          = "${var.vm_name}.qcow2"
}

build {
  sources = ["source.qemu.alpine-qemu-uefi"]

  provisioner "shell" {
    execute_command = "{{.Vars}} doas sh {{.Path}}"
    scripts         = ["scripts/provision-generic.sh"]
  }
}

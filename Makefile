VM_STATE_RUNNING ?= true

all: build_all alpine-qemu alpine-libvirt

build_all: checksum alpine-qemu.pkr.hcl alpine-libvirt.pkr.hcl

alpine-qemu.pkr.hcl:
	@packer validate $@
	@packer build -on-error=abort $@

alpine-libvirt.pkr.hcl:
	@packer validate $@
	@packer build -on-error=abort $@

alpine-qemu:
	@terraform -chdir=./terraform/alpine-qemu init -upgrade -migrate-state
	@terraform -chdir=./terraform/alpine-qemu validate
	@terraform -chdir=./terraform/alpine-qemu apply -var="running=$(VM_STATE_RUNNING)"

alpine-libvirt:
	@terraform -chdir=./terraform/alpine-libvirt init -upgrade -migrate-state
	@terraform -chdir=./terraform/alpine-libvirt validate
	@terraform -chdir=./terraform/alpine-libvirt apply -var="running=$(VM_STATE_RUNNING)"

format-packer:
	@packer fmt -diff -recursive .

format-terraform:
	@terraform fmt -diff -recursive ./terraform/

clean:
	@find ./artifacts/* -prune -exec rm -rfv {} \;

destroy:
	@find ./terraform/* -prune -exec terraform -chdir={} apply -destroy \;

checksum:
	@find . -name *.pkr.hcl -type f -exec sh update_iso_checksum.sh {} \;

.PHONY: all build_all alpine-qemu.pkr.hcl alpine-libvirt.pkr.hcl alpine-qemu alpine-libvirt format-packer format-terraform clean destroy checksum

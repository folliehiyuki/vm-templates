#!/bin/sh

set -e

if [ $# -ne 1 ]; then
	echo "Required only 1 argument."
	exit 1
fi

filename="$1"
if [ ! -r "$filename" ]; then
	echo "File $filename cannot be read."
	exit 1
fi

iso_url=$(hcl2json "$filename" | jq -r '.variable.iso_url[].default')

printf "Checking sha512sum of \033[1;33m%s\033[0m ...\n" "$iso_url"
sha512_sum=$(wget -qO- "${iso_url}".sha512 | awk '{print $1}')
sed -i -E "s|(default += \"sha512:).*\"|\\1$sha512_sum\"|g" "$filename" && printf "ISO checksum updated in \033[1;32m%s\033[0m.\n" "$filename"
